#![feature(proc_macro_hygiene, decl_macro)] // Nightly-only language features needed by Rocket

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_okapi;

use dashmap::DashMap;
use rocket::State;
use rocket::response::status;
use rocket_contrib::json::Json;
use rocket_okapi::JsonSchema;
use serde::*;
use uuid::Uuid;


#[derive(Serialize, Deserialize, Debug, Clone, JsonSchema)]
enum TaskState {
    Ready,
    Assigned,
    Disabled,
    Completed,
    Failed,
}

#[derive(Serialize, Deserialize, Debug, Clone, JsonSchema)]
struct Task {
    id: Uuid,
    payload: String,
    state: TaskState,
}

type TaskMap = DashMap<Uuid, Task>;

/// Submit a new payload for processing
#[openapi]
#[post("/tasks", format="application/octet-stream", data="<payload>")]
fn create_task(tasks: State<TaskMap>, payload: String) -> Result<Json<Task>, status::BadRequest<String>> {
    if payload.len() == 0 {
        return Err(status::BadRequest(Some("payload is empty".to_string())));
    }
    let task = Task {
        id: Uuid::new_v4(),
        payload: payload,
        state: TaskState::Assigned,
    };
    tasks.insert(task.id.clone(), task.clone());
    Ok(Json(task))
}


/// Grab details about an existing task
#[openapi]
#[get("/tasks/<uuid>")]
fn get(tasks: State<TaskMap>, uuid: String) -> Result<Json<Task>, status::NotFound<()>> {
    match Uuid::parse_str(&uuid) {
        Ok(u) => match tasks.get(&u) {
            Some(task) => Ok(Json(task.value().clone())),
            None => Err(status::NotFound(()))
        },
        Err(_) => Err(status::NotFound(()))
    }
}

fn main() {
    let tasks = TaskMap::new();
    rocket::ignite()
        .manage(tasks)
        .mount("/", routes_with_openapi![create_task, get])
        .launch();
}
